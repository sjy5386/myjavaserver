package blocking;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MyListener myListener = new MyListener(12345);
        Thread th = new Thread(myListener);
        th.start();
        while (true) {
            System.out.print(">>");
            String str = scanner.next();
            if (str.equalsIgnoreCase("/close"))
                break;
            myListener.sendAll(str);
        }
        scanner.close();
        System.exit(0);
    }
}
