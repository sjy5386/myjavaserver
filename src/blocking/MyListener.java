package blocking;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class MyListener implements Runnable, Socket2Listener {
    private ServerSocket listener;
    private LinkedList<MySocket> sockets = new LinkedList<>();
    public boolean flag = false;

    public MyListener(int port) {
        try {
            listener = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(int client, String str) {
        MySocket mySocket = sockets.get(client);
        mySocket.send(str);
    }

    public void sendAll(String str) {
        for (MySocket mySocket : sockets) {
            mySocket.send(str);
        }
    }

    @Override
    public void run() {
        flag = true;
        while (flag) {
            try {
                Socket socket = listener.accept();
                MySocket mySocket = new MySocket(socket, this);
                sockets.add(mySocket);
                Thread th = new Thread(mySocket);
                th.start();
                System.out.println(socket.getInetAddress() + ":" + socket.getPort() + " connected.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void disconnected(MySocket mySocket) {
        sockets.remove(mySocket);
    }
}
