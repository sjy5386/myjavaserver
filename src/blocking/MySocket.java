package blocking;

import java.io.*;
import java.net.Socket;

public class MySocket implements Runnable {
    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    private Socket2Listener socket2Listener;

    public MySocket(Socket socket, Socket2Listener socket2Listener) {
        try {
            this.socket = socket;
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.socket2Listener = socket2Listener;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String recv() {
        try {
            String str = in.readLine();
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void send(String str) {
        try {
            out.write(str + "\n");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (socket.isConnected()) {
            String str = recv();
            if (str == null)
                break;
            System.out.println(socket.getInetAddress() + ":" + socket.getPort() + " Client: " + str);
        }
        socket2Listener.disconnected(this);
    }
}
